export type UserType = {
  id: number;
  name: string;
  email: string;
  username: string;
  [key: string]: any;
};

export type PhotoDataType = {
  albumId: number;
  id: number;
  title: string;
  url: string;
  thumbnailUrl: string;
  [key: string]: any;
};

export type AlbumDataType = {
  id: number;
  userId: number;
  title: string;
};

export type ParamsType = {
  albumId?: string | undefined;
  userId?: string | undefined;
};

export type UserIdType = string | undefined;
export type AlbumIdType = string | undefined;
