import axios from "axios";
import { UserIdType,AlbumIdType } from "./definitions";


const API_URL = "https://jsonplaceholder.typicode.com";

export async function getUsersData() {
  try {
    const res = await axios.get(`${API_URL}/users`);
    return res.data;
  } catch (error) {
    console.error("Failed to fetch users:", error);
    throw new Error("Failed to fetch users.");
  }
}

export async function getUserAlbums(userId:UserIdType) {
  try {
    const res = await axios.get(`${API_URL}/users/${userId}/albums`);
    return res.data;
  } catch (error) {
    console.error("Failed to fetch users:", error);
    throw new Error("Failed to fetch users.");
  }
}

export async function getAlbumPhotos(albumId:AlbumIdType) {
  try {
    const res = await axios.get(`${API_URL}/albums/${albumId}/photos`);
    return res.data;
  } catch (error) {
    console.error("Failed to fetch users:", error);
    throw new Error("Failed to fetch users.");
  }
}
