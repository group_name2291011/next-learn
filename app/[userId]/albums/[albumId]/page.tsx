import PhotosContainer from "@/app/Components/photos/PhotosContainer";
import { getAlbumPhotos } from "@/app/utils/data";
import React, { Suspense } from "react";
import { ParamsType } from "./../../../utils/definitions";

const page = async ({ params }: { params: ParamsType }) => {
  const photosData = await getAlbumPhotos(params.albumId);

  return (
    <Suspense fallback={<div>Loading...</div>}>
      <PhotosContainer photosData={photosData} />
    </Suspense>
  );
};

export default page;
