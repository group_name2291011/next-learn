import { Suspense } from "react";
import AlbumsContainer from "../../Components/albums/AlbumContainer";
import { ParamsType } from "./../../utils/definitions";

const page = async ({ params }: { params: ParamsType }) => {
  const { userId } = params ;

  return (
    <Suspense fallback={<div>Loading...</div>}>
      <AlbumsContainer userId={userId} />
    </Suspense>
  );
};

export default page;
