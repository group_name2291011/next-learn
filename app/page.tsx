import UsersContainer from "./Components/users/UsersContainer";
import { Suspense } from "react";

export default function Home() {
  return (
    <main >
      <Suspense fallback={<div>loading...</div>}>
        <UsersContainer />
      </Suspense>
    </main>
  );
}
