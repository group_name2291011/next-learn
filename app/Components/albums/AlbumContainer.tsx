import React from "react";
import { getUserAlbums } from "../../utils/data";
import { UserIdType, AlbumDataType } from "../../utils/definitions";
import AlbumCard from "./AlbumCard";

const AlbumsContainer = async ({ userId }: { userId: UserIdType }) => {
  const data = await getUserAlbums(userId);

  return (
    <>
      <h4 className="text-3xl font-bold mb-4 text-slate-700">
        Available Albums
      </h4>
      <div className="grid gap-16 grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
        {data.map((album: AlbumDataType) => (
          <AlbumCard key={album.title} album={album} />
        ))}
      </div>
    </>
  );
};

export default AlbumsContainer;


