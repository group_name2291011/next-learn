import Image from "next/image";
import { AlbumDataType } from "../../utils/definitions";
import Link from "next/link";

const AlbumCard = ({ album }: { album: AlbumDataType }) => {
    const { id, userId, title } = album;
    return (
      <div className="relative flex flex-col justify-between shadow-md rounded-xl overflow-hidden hover:shadow-lg max-w-sm">
        <Image
          src="https://via.placeholder.com/600/92c952"
          alt={title}
          width={130}
          height={130}
          priority={true}
          className="w-full max-h-32"
        />
        <p className="text-sm px-3 my-2">{title}</p>
        <Link
          href={`/${userId}/albums/${id}`}
          className="bg-slate-400 hover:bg-slate-500 text-white py-2 px-4 rounded flex justify-center align-center"
        >
          View Album
        </Link>
      </div>
    );
  };

  export default AlbumCard;