import Image from "next/image";
import { UserType } from "../../utils/definitions";
import Link from "next/link";


const UserCard = ({user}:{user:UserType})=>{
    const {name, email, id} = user;
    return(
        <div className="relative flex flex-col rounded-xl bg-white bg-clip-border text-gray-700 shadow-md p-4 pb-2 justify-between items-center">
      <Image src={"/avatar.jpg"} alt={name} width={130} height={130} priority={true}/>

      <p className="text-lg font-bold">{name}</p>
      <p className="text-sm">{email}</p>
      <Link href={`/${id}/albums`} className="bg-slate-400 hover:bg-slate-500 text-white py-2 px-4 mt-4 rounded flex justify-center items-center">Explore Albums</Link>
    </div>
  )
}

export default UserCard;