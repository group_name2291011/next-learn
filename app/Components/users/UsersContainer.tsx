import { getUsersData } from "../../utils/data";
import { UserType } from "../../utils/definitions";
import UserCard from "./UserCard";

const UsersContainer = async () => {
  const usersData = await getUsersData();

  return (
    <div className="grid gap-4 grid-cols-3 ">
      {usersData?.map((user: UserType) => (
        <UserCard key={user.name} user={user}/>
      ))}
    </div>
  );
};

export default UsersContainer;


