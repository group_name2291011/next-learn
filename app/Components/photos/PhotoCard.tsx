import Image from "next/image";
import { PhotoDataType } from "../../utils/definitions";
import Link from "next/link";


const PhotoCard = ({
  data,
  slideIndex,
  clickHandler,
}: {
  data: PhotoDataType;
  slideIndex: number;
  clickHandler: (newIndex: number) => void;
}) => {
  return (
    <Link
      href="#"
      onClick={() => clickHandler(slideIndex)}
      className="overflow-hidden w-auto flex flex-col item-center  hover:opacity-70 border rounded border-slate-400"
    >
      <Image
        className="w-full"
        src={data.thumbnailUrl}
        alt={data.title}
        width={150}
        height={150}
        placeholder="blur"
        blurDataURL="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mOsKi2tBwAEugHlrtwQcwAAAABJRU5ErkJggg=="
      />
      <p className="overflow-hidden p-2">{data.title}</p>
    </Link>
  );
};

export default PhotoCard;