"use client";
import React, { useState } from "react";
import { PhotoDataType } from "../../utils/definitions";
import Lightbox from "yet-another-react-lightbox";
import Captions from "yet-another-react-lightbox/plugins/captions";
import Counter from "yet-another-react-lightbox/plugins/counter";
import "yet-another-react-lightbox/styles.css";
import "yet-another-react-lightbox/plugins/captions.css";
import "yet-another-react-lightbox/plugins/counter.css";
import PhotoCard from "./PhotoCard";

const PhotosContainer = ({ photosData }: { photosData: PhotoDataType[] }) => {
  const [index, setIndex] = useState(-1);

  const imgSlides = photosData.map((photo: PhotoDataType) => {
    return { src: photo.url, description: photo.title };
  });

  const clickHandler = (i: number) => {
    setIndex(i);
  };

  return (
    <>
      <div className="grid w-full grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lg:grid-cols-5 gap-6">
        {photosData.map((photo: PhotoDataType, index: number) => (
          <PhotoCard
            key={photo.title}
            data={photo}
            slideIndex={index}
            clickHandler={clickHandler}
          />
        ))}
      </div>

      <Lightbox
        index={index}
        slides={imgSlides}
        open={index >= 0}
        close={() => setIndex(-1)}
        plugins={[Captions, Counter]}
      />
    </>
  );
};

export default PhotosContainer;

